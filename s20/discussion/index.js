/* console.log(`ASAN ANG SABAW!!!!`) */

/*
        - A while loop takes in an expression/condition
        - Expressions are any unit of code that can be evaluated to a value
        - If the condition evaluates to true, the statements inside the code block will be executed
        - A statement is a command that the programmer gives to the computer
        - A loop will iterate a certain number of times until an expression/condition is met
        - "Iteration" is the term given to the repetition of statements
        - Syntax
            while(expression/condition) {
                statement
            }
*/

/* let count = 5;

while (count !== 0) {
    console.log(`While: ${count}`);
    count--;
} */

 // Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
		    // Loops occupy a significant amount of memory space in our devices
		    // Make sure that expressions/conditions in loops have their corresponding increment/decrement operators to stop the loop
		    // Forgetting to include this in loops will make our applications run an infinite loop which will eventually crash our devices
		    // After running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application/browser/tab to avoid this


/* for(let count = 0; count <= 20; count++) {
    console.log(count);
} */

/* let myString = 'awdaweadeaafasw';

console.log(myString.length);

console.log(myString[0]);

for (let i = 0; i < myString.length; i++){
    // console.log(myString[x]);
    let vow = ['a','e','i','o','u'];
    let letter = myString[i].toLowerCase();
    if(vow.includes(letter)) {
        console.log('*');
    } else {
        console.log(letter);
    }
} */

/* for (let count = 0; count <= 20; count++) {

    if (count % 2 === 0){
        continue;
    }
    // Tells the code to continue to the next iteration of the loop
    // This ignores all statements located after the continue statement;
    console.log(`Continue an Break ${count}`);

    // If the current value of count is greater than 10
    // Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
    // number values after 10 will no longer be printed
    if(count > 10){
        break;
    }
} */

let word = 'pneumonoultramicroscopicsilicovolcanoconiosis';

let word1 = 'a';
let word2 = 'b';

let word3 = word1 + word2;
console.log(word3);
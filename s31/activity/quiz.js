// 1. What directive is used by Node.js in loading the modules it needs?
    /* 
        Answer: Require Directive
    
    */


// 2. What Node.js module contains a method for server creation?

    /* 
        Answer: HTTP module
    */

// 3. What is the method of the http object responsible for creating a server using Node.js?

    /* 
        Answer: .createServer()
    */

// 4. What method of the response object allows us to set status codes and content types?
	
    /* 
        Answer: .writeHead()
    */

// 5. Where will console.log() output its contents when run in Node.js?

    /* 
        Answer: Yes and No. Yes, When run using Node.js, console.log's output will be displayed in the terminal. No, it will not be displayed in the browser's dev tools.
    */

// 6. What property of the request object contains the address's endpoint?

    /* 
        Answer: .end()
    */
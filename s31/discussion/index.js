#!/usr/bin/node

import { createServer } from "http";

createServer((request, response) => {
    response.writeHead(200, {"Content-Type" : "text/plain"});
    response.end("hello world"); 
}).listen(4000);


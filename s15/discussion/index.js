//JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables you to create dynamically updating content, control multimedia, animate images
    //Let get started by introducing the basic syntax elements of JavaScript.


    // [SECTION] Syntax, Statements and Comments

    
    //Statements:

    // Statements in programming are instructions that we tell the computer to perform
    // JS statements usually end with semicolon (;)
    // Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
    // A syntax in programming, it is the set of rules that describes how statements must be constructed
    // All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner


    /*
		Where To Place JavaScript
			- Inline You can place JavaScript right into the HTML page using the script tags. This is good for very small sites and testing only. The inline approach does not scale well, leads to poor organization, and code duplication.
			-	External File A better approach is to place JavaScript into separate files and link to them from the HTML page. This way a single script can be included across thousands of HTML pages, and you only have one place to edit your JavaScript code. This approach is also much easier to maintain, write, and debug.
		
		Use of the Script Tag
			In the past, we had to worry about specifying many attributes for the script tag. 

		Where should I place the Script Tags?
			The script tags can go anywhere on the page, but as a best practice, many developers will place it just before the closing body tag on the HTML page. This provides faster speed load times for your web page.

*/

console.log('hi');

// [SECTION] Variables

		// It is used to contain data.
		// Any information that is used by an application is stored in what we call a "memory"
		// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
		// This makes it easier for us associate information stored in our devices to actual "names" about information


		// Declaring variables:

		// Declaring variables - tells our devices that a variable name is created and is ready to store data
		// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".


let firstVariable;

console.log(firstVariable);

// console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console
// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code

// Variables must be declared first before they are used
// Using variables before they're declared will return an error

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

    Best practices in naming variables:

        1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

            let firstName = "Michael"; - good variable name
            let pokemon = 25000; - bad variable name

        2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

            let FirstName = "Michael"; - bad variable name
            let firstName = "Michael"; - good variable name

        3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

            let first name = "Mike";

        camelCase is when we have first word in small caps and the next word added without space but is capitalized:

            lastName emailAddress mobileNumber

        Underscores sample:

        let product_description = "lorem ipsum"
        let product_id = "250000ea1000"

*/

// Declaring and initializing variables
// Initializing variables - the instance when a variable is given it's initial/starting value

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended

// const interest = 3.539;

// Reassigning variable values
// Reassigning a variable means changing it's initial or previous value into another value

productName = "Laptop";
console.log(productName);

/*
When to use JavaScript const?
    As a general rule, always declare a variable with const unless you know that the value will change.

    Use const when you declare:

        -A new Array
        -A new Object
        -A new Function

Note: You can discuss later the constant array and objects once done with the topic of data types. It can only be an overview to explain that the const does not define a constant value. It defines a constant reference to a value so it can change the elements in an array and properties in an object.
*/

//let/const local/global scope
//Scope essentially means where these variables are available for use
//let and const are block scoped
//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.
//So a variable declared in a block with let  is only available for use within that block.
// {}



console.log(Math.PI);

let outerVar = 'hello';

{
    let innerVar = 'hello again';
}

// console.log(innerVar);


let pokemon = 'Pikachu', trainer = "Ash", leve = 7;

// [SECTION] Data Types

    // Strings
    // Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
    // Strings in JavaScript can be written using either a single (') or double (") quote
    // In other programming languages, only the double quotes can be used for creating strings

let country = 'Philppines';
let province = 'cebu';

// Concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAddress = province + ' , ' + country;
console.log(fullAddress);

let greeting = "I live in " + province;
console.log(greeting);

// NUMBER
// whole/number
let headCount = 26;
console.log(headCount);

// decimal/fractions
let heatIndex = 40.1;
console.log(heatIndex);

// exponential notation/Eulers Number
let planetDistance = 2e10;
console.log(planetDistance);

// BOOLEAN
// Boolean values are normally used to store values relating to the state of certain things
// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried = " + isMarried);

// ARRAYS
// Arrays are a special kind of data type that's used to store multiple values
// Arrays can store different data types but is normally used to store similar data types

let grades = [98.7, 92.1, 90.5, 94.6];
console.log(grades);

// different data types
// storing different data types inside an array is not recommended because it will not make sense to in the context of programming
let details = ['John', 'Smith', 32, true];
console.log(details);

// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object

/* 
    Syntax:
        let/const objectName = {
            propertyA : value;
            propertyB : value;
        }
*/

let person = {
    fullName : "Juan Dela Cruz",
    age : 35,
    isMarried : false,
    contact : ['123213213', ],
    address : {
        houseNumber : "456",
        city :"Manila"
    }
};
console.log(person);

let myGrades= {
    firstGrading : 98.7,
    secondGrading : 98.7,
    thirdGrading : 98.7,
    fourthGrading : 98.7,
};
console.log(myGrades);

//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.
console.log(typeof myGrades);

/*
Constant Objects and Arrays
    The keyword const is a little misleading.

    It does not define a constant value. It defines a constant reference to a value.

    Because of this you can NOT:

    Reassign a constant value
    Reassign a constant array
    Reassign a constant object

    But you CAN:

    Change the elements of constant array
    Change the properties of constant object

*/

const animes = ["K On", "Yakitate Japan", "Cyber Kuro-Chan"];
animes[2] = 'Kimetsu no Yaiba';
console.log(animes);

//We can change the element of an array assigned to a constant variable.
//We can also change the object's properties assigned to a constant variable.

// NULL
// It is used to intentionally express the absence of a value in a variable declaration/initialization
// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

let spouse = null;
let myNumber = null;
let myString = null;

// Undifined
// Represents the state of a variable that has been declared but without an assigned value
let fullName;
let feelings;
console.log(feelings);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
// null means that a variable was created and was assigned a value that does not hold any value/amount
// Certain processes in programming would often return a "null" value when certain tasks results to nothing
let varA =
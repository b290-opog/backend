// Controllers contain the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file
const Task = require("../models/Task.js");

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => result)
};

// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (reqBody) => {
    console.log(reqBody);
    // Creates a task object based on the Mongoose model "Task"
    let newTask = new Task({
        // Sets the "name" property with the value received from the client/Postman
        name : reqBody.name
    });

    return newTask.save().then((task, error) => {
        if(error){
            console.log(error);
            return false
        } else {
            return task
        }
    });
};

module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then(removedTask => {
        return removedTask;
    }).catch(err => {
        console.log(err);
        return false
    })
};

// Controller function for updating a task

    // Business Logic
    /*
        1. Get the task with the id using the Mongoose method "findById"
        2. Replace the task's name returned from the database with the "name" property from the request body
        3. Save the task
    */

    // The task id retrieved from the "req.params.id" property coming from the client is renamed as a "taskId" parameter in the controller file
    // The updates to be applied to the document retrieved from the "req.body" property coming from the client is renamed as "newContent"
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then(result => {
        result.name = newContent.name;
        return result.save().then((updatedTask, saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false
            } else {
                return updatedTask;
            }
        })
    }).catch(err => console.log(err));
};


/* ACTIVITY */

// GETTING A SPECIFIC TASK
module.exports.getSpecificTask = (taskId) => {
    return Task.findById(taskId).then(result => result)
};


// UPDATING A SPECIFIC TASK'S STATUS
module.exports.updateTaskStatus = (taskId) => {
    return Task.findById(taskId).then(result => {
        result.status = "complete";
        return result.save().then((updatedTaskStatus, saveErr) => {
            if(saveErr){
                console.log(saveErr);
                return false
            }else{
                return updatedTaskStatus;
            }
        })
    }).catch(err => console.log(err));
};
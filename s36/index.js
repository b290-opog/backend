// set-up dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute.js")

// server set-up
const app = express();
const port = 4000;

// DB Connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.mo2vnle.mongodb.net/b290-to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
    }
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("MongoDB Atlas connected."));

// set-up Middleware
app.use(express.json());
app.use(express.urlencoded({ extended : true }));


// set-up Routes
app.use("/tasks", taskRoute);


if(require.main === module){
    app.listen(port, () => console.log(`Server running at port ${port}`))
};

module.exports = app;
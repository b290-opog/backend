// console.log(`let there be light`)

/* function printInput(){
    let nickName = prompt('Enter your nickname.');
    return `Hi ${nickName}`;
}

console.log(printInput()); */

/* 
You can directly pass data into the function. The function can then call/use that data which is referred as "name" within the function.

"name" is called a parameter.

A "parameter" acts as a named variable/container that exists only inside of a function
It is used to store information that is provided to a function when it is called/invoked.

*/

function printName(name){
    return `My name is ${name}`;
}

/* 
the information/data provided directly into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

*/


// console.log(printName('asdas'));
/* 
function printInput() {
    nickname = 
} */


function tryMe(){
	console.log("Gulat ka noh?");
}

function argumentFunction(){
    console.log(`This function was passed as an argument before it was printed`);   
}

function invokeFunction(args){
    args();
}

invokeFunction(tryMe);


function createFullName(firstName, middleName, lastName){
    return `${firstName} ${middleName} ${lastName}`;
}

console.log(createFullName("Juan", "Dela", "Cruz", "jr"))
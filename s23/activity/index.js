// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
let trainer = {
    name: "Ash Ketcup",
    age: 10,
    pokemon: ['Pikachu', 'Butterfree', 'Lapras', 'Charizard'],
    friends: {
        hoen: ['May', 'Max'],
        kanto: ['Brock', 'Misty']
    }
}
// Initialize/add the given object properties and methods
trainer.talk = function(){
    return `Pikachu! I choose you!`;
};
// Properties
console.log(trainer);

// Methods

// Check if all properties and methods were properly added
// Access object properties using dot notation
console.log(`Results of dot notation:`);
console.log(trainer.name);

// Access object properties using square bracket notation
console.log(`Results of square bracket notation:`);
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log(`Results of talk Method:`);
console.log(trainer.talk())


// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level){
    this.name = name,
    this.level = level,
    this.health = 2 * level,
    this.attack = level,
    this.tackle = function(target){
        target.health -= this.attack;

        if(target.health <= 0 ){
            return  `${this.name} tackled ${target.name} \n ${target.name}'s health is now reduced to ${target.health -= this.attack}\n ${target.faint()}`;
        } else {
            return `${this.name} tackled ${target.name} \n ${target.name}'s health is now reduced to ${target.health -= this.attack}`;
        };
    },
    this.faint = function(){
        return(`${this.name} fainted`);
    }
}

// Create/instantiate a new pokemon
let Pichu = new Pokemon('Pichu', 15);
console.log(Pichu);

// Create/instantiate a new pokemon
let Eevee = new Pokemon('Eevee', 25);
console.log(Eevee);


// Create/instantiate a new pokemon
let Arceus = new Pokemon('Arceus', 100);
console.log(Arceus);

// Invoke the tackle method and target a different object
console.log(Arceus.tackle(Pichu));
console.log(Pichu);

// Invoke the tackle method and target a different object
console.log(Pichu.tackle(Eevee));
console.log(Eevee);









//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}
// CRUD Operation

// [SECTION] Inserting documents (create)
// Syntax: db.collectionName.insertOne({object})

db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "09123456789",
        email: "janedoe@gmail.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
});

// Inserting multiple documents
// Syntax: db.collectionName.insertMany([{objectA}, {objectB}]);
db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "09123456789",
            email: "stephenhawking@gmail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Niel",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "09123456789",
            email: "nielarmstrong@gmail.com"
        },
        courses: ["React", "Laravel", "Sass"],
        department: "none"
    }
]);

// SECTION - Finding documents (read)
/* 
    Syntax:
    db.collectionName.find();
    db.collectionName.find({field: value})

*/

db.users.find();

db.users.find({ firstName: "Stephen"});

// Finding documents with multiple parameters
// db.collectionName.find({field: value, field: value} )

db.users.find({  lastName: "Armstrong", age: 82});


//[SECTION] Updating documents (Read)
// Updating a single document
// Syntax: db.collectionName.updateOne({criteria}, {$set: {field: value}})

// Document to be updated
db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});

db.users.updateOne(
    {firstName: "Test"},
    {
        $set: {
            firstName: "Bill",
            lastName: "Gates",
            age: 65,
            contact: {
                phone: "09123456789",
                email: "bill@gmail.com"
            },
            courses: ["PHP", "Laravel", "HTML"],
            department: "Operations",
            status: "active"
        }
    }
);

db.users.find({firstName: "Bill"});

// Updating multiple documents
/* 
    syntax:
        db.collectionName.updateMany({criteria}, {$set: {fieldA: value}})
*/

db.users.updateMany(
    { department: "none"},
    {
        $set: { department: "HR"}
    }
);

// Replace One
db.users.replaceOne(
    { firstName: "Bill" },
    {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "09123456789",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
    }
);

db.users.insertOne({
    firstName: "test"
})

db.users.deleteOne({firstName: "test"})


db.users.deleteMany({firstName: "Bill"});

// ADVANCE Queries
db.users.find({
    contact: {
        phone: "09123456789",
        email: "nielarmstrong@gmail.com"
    }
})

db.users.find({"contact.email" : "janedoe@gmail.com"})


db.users.find({courses : ["CSS", "JavaScript", "Python"]});
db.users.find({courses : {$all: ["React", "Python"]}});

db.users.insertOne({
    namearr: [
        {
            namea: "Juan"
        },
        {
            nameb: "Tamad"
        }
    ]
})

db.users.find({
    namearr: {
        namea: "juan"
    }
})
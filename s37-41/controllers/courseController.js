const Course = require("../models/Course");
const auth = require("../auth");

// Create a new course
    /*
        Steps:
        1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
        2. Save the new User to the database
    */

module.exports.addCourse = (reqBody) => {
        let newCourse = new Course({
            name : reqBody.name,
            description : reqBody.description,
            price : reqBody.price,
        });

        // Save the Created object "newCourse" to our database
        return newCourse.save().then(course => true).catch(err => {
            console.log(err);
            return false;
        });
};


module.exports.getAllCourses = () => {
    return Course.find({}).then(result => result).catch(err => {
        console.log(err);
        return false;
    });
};

module.exports.getAllActiveCourses = () => {
    return Course.find({ isActive : true }).then(result => result).catch(err => {
        console.log(err);
        return false;
    });
};

module.exports.getCourse = reqParams => {
    return Course.findById(reqParams.courseId).then(result => result).catch(err => {
        console.log(err);
        return false;
    })
};

module.exports.updateCourse = (reqParams, reqBody) => {
   let updatedCourse = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
   };

   return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(course => true).catch(err => {
        console.log(err);
        return false;
   });
};

module.exports.archiveCourse = (reqParams, reqBody) => {
    let archivedCourse = {
        isActive : reqBody.isActive
    };

    return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then(course => true).catch(err => {
        console.log(err);
        return false;
    });
};
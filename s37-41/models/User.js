const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [ true, "Firstname is required"]
    },
    lastName : {
        type : String,
        required : [ true, "Lastname is required"]
    },
    email : {
        type : String,
        required : [ true, "Email is required"]
    },
    password : {
        type : String,
        required : [ true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String,
        required : [ true, "Mobile number is required"]
    },
    enrollments : [
        {
            courseId : {
                type : String,
                required : [ true, "CourseId is requried"]
            },
            enrolledOn : {
                type : Date,
                default : new Date()
            },
            status : {
                type : String,
                default : "Enrolled"
            }
        }
    ]
});

module.exports = mongoose.model("User", userSchema);
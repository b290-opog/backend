// console.log("kahit anu jan");

// [SECTION] ARITHMETIC OPERATORS

    let x = 1397;
    let y = 7831;

    let sum = x + y;
    console.log("Result of addition operator: " + sum);

    // MINI ACTIVITY

    let diff = x - y;
    console.log(`Result of Subtraction operator: ${diff}`);

    let prod = x * y;
    console.log(`Result of Multiplication operator: ${prod}`);

    let qout = x / y;
    console.log(`Result of Division operator: ${qout}`);

    let modulos = y % x;
    console.log(`Result of Modulo operator: ${modulos}`);


// [SECTION] Assignment Operators
    // Basic Assignment Operator
    // The assignment operator assigns the value of the **right** operand to a variable.

    let assignmentNumber = 8;

    // Addition Assignment Operator (+=)
    // The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
    assignmentNumber = assignmentNumber + 2;
    console.log("Result of Addition assignment operator " + assignmentNumber);

    // Shorthand for assignmentNumber = assignmentNumber + 2;
    assignmentNumber += 2;
    console.log("Result of Addition assignment operator " + assignmentNumber);

    // MINI ACTIVITY

    assignmentNumber -= 2;
    console.log("Result of Subtraction assignment operator " + assignmentNumber);

    assignmentNumber *= 2;
    console.log("Result of Multiplication assignment operator " + assignmentNumber);

    assignmentNumber /= 2;
    console.log("Result of Division assignment operator " + assignmentNumber);

    
    assignmentNumber %= 2;
    console.log("Result of Modulo assignment operator " + assignmentNumber);


// Multiple Operators and Parentheses
/* 
    - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
        - The operations were done in the following order:
            1. 3 * 4 = 12
            2. 12 / 5 = 2.4
            3. 1 + 2 = 3
            4. 3 - 2.4 = 0.6
*/

    let mdas = 1 + 2 - 3 * 4 / 5;

    console.log(`Result of MDAS Operation: ${mdas}`);

    let pmdas = 1 + (2 - 3) * (4 / 5);
/*
- By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
- The operations were done in the following order:
    1. 4 / 5 = 0.8
    2. 2 - 3 = -1
    3. -1 * 0.8 = -0.8
    4. 1 + -.08 = .2
*/
    console.log(`Result of PMDAS Operation: ${pmdas}`);

    pmdas = (1 + (2 - 3)) * (4 / 5);
/*
    - By adding parentheses "()" to create more complex computations will change the order of operations still following the same rule.
    - The operations were done in the following order:
        1. 4 / 5 = 0.8
        2. 2 - 3 = -1
        3. 1 + -1 = 0
        4. 0 * 0.8 = 0
*/
    console.log(`Result of PMDAS Operation: ${pmdas}`);

// Inrement & Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: " + increment);
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
console.log("Result of pre-increment: " + z);

// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one
increment = z++;
// The value of "z" is at 2 before it was incremented
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

let decrement = --z;
// The value of "z" is at 3 before it was decremented
console.log("Result of pre-decrement: " + decrement);
// The value of "z" was decreased reassigning the value to 2
console.log("Result of pre-decrement: " + z);

decrement = z--;
// The value of "z" is at 3 before it was decremented
console.log("Result of post-decrement: " + decrement);
// The value of "z" was decreased reassigning the value to 1
console.log("Result of post-decrement: " + z);

// [SECTION] | COMPARISON OPERATORs
    let juan = 'juan';

    // (Loose) Equality operator
    console.log(1 == 1);
    console.log(1 == 2);
    console.log(1 == '1');
    console.log(0 == false);
    console.log("juan" == 'juan');
    console.log("juan" == juan);

    // Strict Equality Operator
    /* 
        - Checks whether the operands are equal/have the same content
        - Also COMPARES the data types of 2 values
        - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
        - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
        - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
        - Strict equality operators are better to use in most cases to ensure that data types provided are correct
    */

    console.log(1 === 1);
    console.log(1 === 2);
    console.log(1 === '1');
    console.log(0 === false);
    console.log('juan' === 'juan');
    console.log("juan" === juan);

    // Loose Inequlity Operator

    console.log(1 != 1);
    console.log(1 != 2);
    console.log(1 != '1');
    console.log(0 != false);
    console.log("juan" != 'juan');
    console.log("juan" != juan);

    // Strict Inequality Operator
    /* 
        - Checks whether the operands are not equal/have the same content
        - Also COMPARES the data types of 2 values
    */

    console.log(1 !== 1);
    console.log(1 !== 2);
    console.log(1 !== '1');
    console.log(0 !== false);
    console.log('juan' !== 'juan');
    console.log("juan" !== juan);

    // [SECTION] Relational Operators
    //Some comparison operators check whether one value is greater or less than to the other value.

    let a = 50;
    let b = 65;

    // GT Operator (>)
    let isGreaterThan = a > b;
    // LT Operator (<)
    let isLessThan = a < b;
    // GTE Operator (<=)
    let isGTorEqual = a >= b;
    // LTE Operator (<=)
    let isLTorEqual = a <= b;

    console.log(isGreaterThan);
    console.log(isLessThan);
    console.log(isGTorEqual);
    console.log(isLTorEqual);

    let numStr = '30';
    console.log(a > numStr); //true = converted to a number
    console.log(b <= numStr);

    let str = "twenty";
    console.log(b > str); //false = str is not a number

// [SECTION] Logical Operators
    let isLegalAge = true;
    let isRegistered = false;

    // AND Operator (&& - Double Ampersand)
    // Returns true if all operands are true
    let allRequirementsMet = isLegalAge && isRegistered;
    console.log("Result of logical AND Operator: " + allRequirementsMet);

    // OR Operator (|| Double Pipe)
    // Returns true if one of the operands are true
    let someRequirementsMet = isLegalAge || isRegistered;
    console.log("Result of logical OR Operator: " + someRequirementsMet);

    // NOT Operator (! Exclamation Point)
    // Returns the opposite value
    let someRequirementsNotMet = !isRegistered;
    console.log("Result of logical NOT Operator: " + someRequirementsNotMet);





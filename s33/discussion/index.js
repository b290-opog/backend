/* console.log("asan ang sabae!!!") */

// [Section] Javascript Synchronous vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed at a time

/* console.log(`Hello`);
// conosole.log(`Hello`);
console.log(`Bye`);

for(let i = 0; i <= 1500; i++){
    console.log(i);
};
console.log("hellow"); */

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// [Section] Getting all posts

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
/* 
    Syntax:
        fetch("URL")

*/

// console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

// fetch("https://jsonplaceholder.typicode.com/posts")
// .then(response => console.log(response.status));

// fetch("https://jsonplaceholder.typicode.com/posts")
//     .then(response => response.json())
//     .then(json => console.log(json));

// // The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// // Used in functions to indicate which portions of code should be waited for


// // Creates an Async function
// async function fetchData(){

//     // waits for the "fetch" method to complete then stores the value in the "result" variable
//     let result = await fetch("https://jsonplaceholder.typicode.com/posts");
//     console.log(result);
//     console.log(typeof result);
//     console.log(result.body);
//     let json = await result.json();
//     console.log(json);
// }

// fetchData();


// fetch("https://jsonplaceholder.typicode.com/posts/1")
//     .then(response => response.json())
//         .then(json => console.log(json));



// fetch("https://jsonplaceholder.typicode.com/posts",
//         {
//             // Sets the method of the "Request" object to "POST" following REST API
// 			// Default method is GET
//             method : "POST",
//             // Sets the header data of the "Request" object to be sent to the backend
// 			// Specified that the content will be in a JSON structure
//             // Sets the content/body data of the "Request" object to be sent to the backend
// 			// JSON.stringify converts the object data into a stringified JSON
//             headers : {
//                 "Content-Type" : "application/json"
//             },
//             body : JSON.stringify({
//                 title : "New Post",
//                 body : "Hello World",
//                 userId : 1
//             })
//         }
//     ).then(response => response.json())
//         .then(json => console.log(json));

// fetch("https://jsonplaceholder.typicode.com/posts/1",
//         {
//             // Sets the method of the "Request" object to "POST" following REST API
// 			// Default method is GET
//             method : "PUT",
//             // Sets the header data of the "Request" object to be sent to the backend
// 			// Specified that the content will be in a JSON structure
//             // Sets the content/body data of the "Request" object to be sent to the backend
// 			// JSON.stringify converts the object data into a stringified JSON
//             headers : {
//                 "Content-Type" : "application/json"
//             },
//             body : JSON.stringify({
//                 id : 1,
//                 title : "Updated post",
//                 body : "Hello again",
//                 userId : 1

//             })
//         }
//     ).then(response => response.json())
//         .then(json => console.log(json));


// // Patch
// fetch("https://jsonplaceholder.typicode.com/posts/1",
//         {
           
//             method : "PATCH",
           
//             headers : {
//                 "Content-Type" : "application/json"
//             },
//             body : JSON.stringify({
//                 body : "This was Patched",
//             })
//         }
//     ).then(response => response.json())
//         .then(json => console.log(json));


// // DELETE
// fetch("https://jsonplaceholder.typicode.com/posts/1",
//         {
//             method : "DELETE",
//         }
// );

// // The data can be filtered by sending the userId along with the URL
// // Information sent vie the url can be done by adding the question mark symbol (?)

// /* 
//         Syntax: 
//             Individual Parameter
//             "url?parameterName=value"
//             Multiple Parameter
//             "url?paramA=value&paramB=value"
// */

// fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
//         .then(response => response.json())
//             .then(json => console.log(json));
        
// // [Section] Retreving nested/related comments to posts
// fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
//         .then(response => response.json())
//             .then(json => console.log(json));





// ACTIVITY teSt
fetch("https://jsonplaceholder.typicode.com/todos")
        .then(response => response.json())
            .then(json => json.map(items => items.title))
                .then(mapped => console.log(mapped[0]));


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
    .then(json => console.log(json)).then(()=> console.log(`from todos 1`));


    fetch("https://jsonplaceholder.typicode.com/todos",
            {
                method : "POST",
                headers : {
                    "Content-Type" : "application/json"
                },
                body : JSON.stringify({
                    completed : false,
                    id : 201,
                    title : "Created To Do List Items",
                    userId : 1
                })
            }
        ).then(response => response.json())
            .then(json => console.log(json));


    fetch("https://jsonplaceholder.typicode.com/todos/1",
            {
                method : "PUT",
                headers : {
                    "Content-Type" : "application/json"
                },
                body : JSON.stringify(
                  {
                    title : "Updated To Do List Items",
                    description : "To update the my todo list with a different date.",
                    status : "Pending",
                    dateCompleted : "Pending",
                    userId : 1
                  })
            }
            ).then(response => response.json())
               .then(json => console.log(json))

    fetch("https://jsonplaceholder.typicode.com/posts/1",
            {
            method : "DELETE",
            }
            ).then(response => response.json())
            .then(json => console.log(json))